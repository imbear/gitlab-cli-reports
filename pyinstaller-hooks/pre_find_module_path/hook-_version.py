import os
from _version import __version__

def pre_find_module_path(pfmp_api):
  # Bake the version information into the executable.
  os.rename('_version.py', '_version.py.bak')
  f = open('_version.py', 'w+')
  f.write("__version__ = '%s'" %(__version__))
  f.close()
